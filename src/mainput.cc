#include <iostream>
#include <opendht.h>
#include <czmq.h>
#include "common.h"


int main(int argc, char** argv)
{
   
    std::cout << "Start DHT node." << std::endl;
    dht::DhtRunner dhtNode;
    //dhtNode.run(ODHT_PORT, dht::crypto::generateIdentity(), true);
    dhtNode.run(ODHT_PORT, {}, true);

    std::string key = "TestKey";
    std::string data = "Data";
    std::string data2 = "Data2";

                // Convert the value to bytes
    std::vector<uint8_t> opendht_data(data.begin(), data.end());
    std::vector<uint8_t> opendht_data2(data2.begin(), data2.end());
    
    auto keyhash = dht::InfoHash::get(key);

    dht::ValueType vt(1, "R", std::chrono::minutes(1));
    dhtNode.registerType(vt);


    //dhtNode.put(keyhash, dht::Value(vt, opendht_data));
    
    int counter = 0;
    while(!zsys_interrupted){
        sleep(1);
        //std::cout << "Wait.." << std::endl;
        //std::cout << dhtNode.getSearchesLog(AF_INET);
        std::cout << dhtNode.getStorageLog() << std::endl;

        //auto exp = dhtNode.exportValues();

        //for (int i =0; i<exp.size(); i++){
        //    dht::ValuesExport pair = exp[i];
        //    std::cout << std::string(pair.second.begin(), pair.second.end()) << std::endl;
        //}

        if (counter%5 == 0){
            dhtNode.get(key,
                [](const std::vector<std::shared_ptr<dht::Value>>& values) {
                    
                    if (values.size()>0){
                        for (const auto& v : values) {
                            std::string result = std::string(v->data.begin(), v->data.end());
                            std::cout << v << "; ";
                        }
                        std::cout << std::endl;
                    }
                    else {
                        std::cout << "No results " << std::endl;
                    }
                    return true;
                },
                [](bool success) {
                    std::cout << "Get finished with " << (success ? "success" : "failure") << std::endl;
                }
                );
        }

        if (counter++<40){
            std::string d = std::string("D") + std::to_string(counter);
            std::vector<uint8_t> o(d.begin(), d.end());
            dht::time_point now = std::chrono::steady_clock::now();
            dhtNode.put(keyhash, dht::Value(vt, o), [](bool success){}, now);
        }

        //if (counter++==20){
        //    dhtNode.put(keyhash, dht::Value(opendht_data2));
       // }
        //else
        //if (counter++ == 40){
        //    break;
       // }
    }

    dhtNode.join();

}
