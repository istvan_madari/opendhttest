#include <iostream>
#include <opendht.h>
#include <czmq.h>
#include "common.h"


int main(int argc, char** argv)
{
    if (argc!=2){
        std::cout << "Missing target ip" << std::endl;
        return -1;
    }

    char* target = argv[1];

    std::string key = "TestKey";
    //std::string data = "Data";

    std::cout << "Start DHT node." << std::endl;
    dht::DhtRunner dhtNode;
    //dhtNode.run(ODHT_PORT, dht::crypto::generateIdentity(), true);
    dhtNode.run(ODHT_PORT, {}, true);
    
    dhtNode.bootstrap(target, std::to_string(ODHT_PORT));

    //dht::ValueType vt(1, "R", std::chrono::minutes(2));
    //dhtNode.registerType(vt);

    std::map<dht::NodeStatus, std::string> nodeStatusStr = {
        {dht::NodeStatus::Connected, "Connected"},
        {dht::NodeStatus::Connecting, "Connecting"},
        {dht::NodeStatus::Disconnected, "Disconnected"}};

    dhtNode.setOnStatusChanged([&nodeStatusStr](dht::NodeStatus ns1, dht::NodeStatus ns2){
        std::cout << "Status changed INET: " << (nodeStatusStr[ns1]) << std::endl; // << " INET6: " << nodeStatusStr[ns2] << std::endl;
    });

    /*dhtNode.listen(key, [](const std::vector<std::shared_ptr<dht::Value>> &values) {
        for (const auto& v : values) {
            std::string result = std::string(v->data.begin(), v->data.end());
            std::cout << "RAW: " << v << "Value: " << result << "; ";
        }
        std::cout << std::endl;
        return true;
    });*/
    
    
    
    while(!zsys_interrupted){

        dhtNode.get(key,
            [](const std::vector<std::shared_ptr<dht::Value>>& values) {
                if (values.size()>0){
                    for (const auto& v : values) {
                        std::string result = std::string(v->data.begin(), v->data.end());
                        std::cout << result << "; ";
                    }
                    std::cout << std::endl;
                }
                else {
                    std::cout << "No results " << std::endl;
                }
                return true;
            }//,
            //[](bool success) {
            //    std::cout << "Get finished with " << (success ? "success" : "failure") << std::endl;
            //}
            );
         
        std::cout << "Zzzz..." << std::endl;
        
        sleep(5);

    }

    dhtNode.join();

}
